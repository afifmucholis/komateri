<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Upload extends CI_Controller {    
    public function __construct() {
        parent::__construct();   
        if (!$this->session->userdata('logged_in'))
        {
            redirect(base_url());
        }
        $id = $this->session->userdata('username');
        $this->load->model('Tambah_User');
        $data['user_detail'] = $this->Tambah_User->user_detail($id);
        $this->load->view('ckepala', $data);
        $this->load->library('upload');

    }       
    public function file_view(){
        $this->load->view('file_view', array('error' => ' ' ));    
    }

    function hashpassword($password) {
        return md5($password);
    }

    public function upload_file()
    {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';



        if ($status != "error")
        {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($file_element_name))
            {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }
            else
            {
                $dat = $this->upload->data();
                $id = $this->input->post('id');

                $data['username'] = $this->input->post('username');
                $data['password'] = $this->hashpassword($this->input->post('password'));
                $data['nama'] = $this->input->post('nama');
                $data['email'] = $this->input->post('email');
                $data['jk'] = $this->input->post('jk');
                $data['level'] = $this->input->post('level');
                $data['telpon'] = $this->input->post('telpon');
                $data['deskripsi'] = $this->input->post('deskripsi');
                $data['foto'] = $dat['file_name'];

                $data['alamat'] = $this->input->post('alamat');
                $this->load->model('user');
                $file_id = $this->user->edit($data, $id);

                if($file_id)
                {

                    $status = "succes";
                    $msg = "Data berhasil diubah";
                    redirect($_SERVER['HTTP_REFERER']);  
                }
                else
                {
                //unlink($data['full_path']);
                    $status = "error";
                    $msg = "Ada belum beruntung, silahkan coba lagi";

                }
            }
            @unlink($_FILES[$file_element_name]);
        }
   // echo json_encode(array('status' => $status, 'msg' => $msg));
        echo '<script language="javascript">';
        echo "alert('<?=$msg?>');back();back()";
        echo '</script>';
        $this->load->model('Tambah_User');
        
        $id = $this->session->userdata('username');
        $data['user_detail'] = $this->Tambah_User->user_detail($id);
        //$this->load->view('ckepala', $data);
        
        $id = $this->session->userdata('username');
        $data['user_detail'] = $this->Tambah_User->user_detail($id);
        $this->load->view('ubah_profil', $data);

    }
}
?>