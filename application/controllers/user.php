<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in'))
			{
				redirect(base_url());
			}
		if ($this->session->userdata('level') != 1)
			{
				redirect('contributor');
			}
			$id = $this->session->userdata('username');
		$this->load->library("Nusoap_library", "");
		$this->load->model('Tambah_User');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		$this->load->view('kepala', $data);
		$this->load->library('upload');

    }

    public function nusoap()
    {
		$ini = $this->session->userdata;
		$data['username'] = $ini['username'];
    	$p = array(
    		'id_post' => $this->input->post('id_post')
    		);
    	$client = new soapclient("http://reggae.id/sit/server.php?wsdl");
		$berita = $client->artikel($p);
		foreach ($berita as $key => $value) {
			$data['kode_kategori'] = $value->kategori;
			$data['judul'] = $value->judul;
			$data['isi'] = $value->isi."penulis ".$value->penulis;
			
		}

			$this->load->model('Materi');
			if($this->Materi->simpan($data)) {
				redirect('user/lihatMateri', 'refres');  
			} else {
				echo "input data tidak berhasil";
			}
    }

	public function index() {
		
		$this->load->view('index');
		$this->load->view('kaki');
	}

	function hashpassword($password) {
        return md5($password);
    }

	public function tambah() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
			//$this->form_validation->set_rules('no_loker', 'Nomor Loker', 'required|alpha_numeric');
		
			//if ($this->input->post('submit')) {
		if($this->form_validation->run()){
			$data['username'] = $this->input->post('username');
			$data['password'] = $this->hashpassword($this->input->post('password'));
			
			$data['level'] = $this->input->post('level');
			$this->load->model('Tambah_User');

			

			if($this->Tambah_User->simpan($data)) {
				$this->index();
			} else {
				echo "input data tidak berhasil";
			}

		} else {
			$this->load->view('tambah');
		}


	}

	public function isi(){

		$this->load->model('Tambah_User');

		$data['user'] = $this->Tambah_User->daftar();

		$this->load->view('daftar', $data);

		
	}

	public function hapus(){
		$id = $this->uri->segment(3);

		$this->load->model('Tambah_User');
		$this->Tambah_User->hapus($id);
		redirect($_SERVER['HTTP_REFERER']);  
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	public function tambahMateri(){
		$this->form_validation->set_rules('kode_kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('judul', 'Judul', 'required');
			
		if($this->form_validation->run()){
			$ini = $this->session->userdata;
			$data['username'] = $ini['username'];
			$data['kode_kategori'] = $this->input->post('kode_kategori');
			$data['judul'] = $this->input->post('judul');
			$data['isi'] = $this->input->post('isi');
			//$data['level'] = $this->input->post('level');
			
			$this->load->model('Materi');
			if($this->Materi->simpan($data)) {
				redirect('user/lihatMateri', 'refres');  
			} else {
				echo "input data tidak berhasil";
				
			}

		} else {
			$this->load->model('Materi');
        	$data['kategori'] = $this->Materi->lihatKategori();
			$this->load->view('tambahMateri', $data);
		}

		
	}

	public function lihatMateri(){

		$this->load->model('Materi');
		$data['materi'] = $this->Materi->daftarlagi();

		
		$this->load->view('lihatMateri', $data);		
	}

	public function hapusmateri(){
		$id = $this->uri->segment(3);

		$this->load->model('Materi');
		$this->Materi->hapusmateri($id);
		redirect($_SERVER['HTTP_REFERER']);  
	}

	public function lihatKategori(){
		$this->load->model('Materi');
		$data['kategori'] = $this->Materi->lihatKategori();

		
		$this->load->view('lihatKategori', $data);	
	}

	public function tambahKategori(){
		$this->form_validation->set_rules('kode_kategori', 'Kode Kategori', 'required');
		
			//$this->form_validation->set_rules('no_loker', 'Nomor Loker', 'required|alpha_numeric');
		
			//if ($this->input->post('submit')) {
		if($this->form_validation->run()){
			
			$data['kode_kategori'] = $this->input->post('kode_kategori');
			$data['nama_kategori'] = $this->input->post('nama_kategori');

			$this->load->model('Materi');

			if($this->Materi->simpanKategori($data)) {
				
			$this->load->view('tambahKategori');

			} else {
				echo "input data tidak berhasil";
			}

		} else {
			
			$this->load->view('tambahKategori');
		}
		
	}

	public function hapusKategori(){
		$id = $this->uri->segment(3);
		
		$this->load->model('Materi');
		$this->Materi->hapusKategori($id);
		redirect($_SERVER['HTTP_REFERER']);  
	}

	public function punyaku()
	{
		$username = $this->session->userdata('username');

		$this->load->model('Materi');

		$data['materi'] = $this->Materi->clihatMateri($username);

		
		$this->load->view('punyaku', $data);	
	}

	public function edit(){
		$id = $this->uri->segment(3);	
		$this->load->model('Materi');
		$data['kategori'] = $this->Materi->lihatKategori();
		$data['materi'] = $this->Materi->editMateri($id);	
		$this->load->view('edit', $data);	
	}

	public function update(){
		$data['id_materi'] = $this->input->post('id_materi');
		$data['kode_kategori'] = $this->input->post('kode_kategori');
		$data['judul'] = $this->input->post('judul');
		$data['isi'] = $this->input->post('isi');
		$this->load->model('Materi');
		if($this->Materi->updateMateri($data, $data['id_materi'])) {
			redirect('user/lihatMateri', 'refres');
		} else {
			echo "input data tidak berhasil";

		}
	}

	public function editKategori(){
		$id = $this->uri->segment(3);
		$this->load->model('Materi');
		$data['kategori'] = $this->Materi->editKategori($id);
		$this->load->view('editKategori', $data);		
	}

	public function updateKategori(){
		$id = $this->input->post('id');
		$data['kode_kategori'] = $this->input->post('kode');
		$data['nama_kategori'] = $this->input->post('nama');
		
		$this->load->model('Materi');
		if($this->Materi->updateKategori($data, $id)) {
			redirect('user/lihatKategori', 'refres');
		} else {
			echo "input data tidak berhasil";

		}
	}

	public function ubah_profil(){
		$id = $this->session->userdata('username');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		//$this->load->view('ckepala', $data);
		
		$id = $this->session->userdata('username');
		$this->load->model('Tambah_User');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		$this->load->view('ubah_profil', $data);
	}	

}