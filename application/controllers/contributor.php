<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contributor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in'))
		{
			redirect(base_url());
		}
		if ($this->session->userdata('level') != '0')
		{
			redirect('user');
		}

		$id = $this->session->userdata('username');
		$this->load->model('Tambah_User');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		$this->load->view('ckepala', $data);
		$this->load->library('upload');

	}

	public function index() {
		
		$this->load->view('cindex');
		$this->load->view('kaki');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	public function tambahMateri(){
		$this->form_validation->set_rules('kode_kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('judul', 'Judul', 'required');

		if($this->form_validation->run()){
			$ini = $this->session->userdata;
			$data['username'] = $ini['username'];
			$data['kode_kategori'] = $this->input->post('kode_kategori');
			$data['judul'] = $this->input->post('judul');
			$data['isi'] = $this->input->post('isi');
			//$data['level'] = $this->input->post('level');
			
			$this->load->model('Materi');
			if($this->Materi->simpan($data)) {
				$this->index();
			} else {
				echo "input data tidak berhasil";
				
			}

		} else {
			$this->load->model('Materi');
			$data['kategori'] = $this->Materi->lihatKategori();
			$this->load->view('ctambahMateri', $data);
		}

		
	}

	public function lihatMateri(){
		$username = $this->session->userdata('username');

		$this->load->model('Materi');

		$data['materi'] = $this->Materi->clihatMateri($username);

		
		$this->load->view('clihatMateri', $data);		
	}

	public function lihatKategori(){
		$this->load->model('Materi');
		$data['kategori'] = $this->Materi->lihatKategori();

		
		$this->load->view('clihatKategori', $data);	
	}

	public function tambahKategori(){
		$this->form_validation->set_rules('kode_kategori', 'Kode Kategori', 'required');
		
			//$this->form_validation->set_rules('no_loker', 'Nomor Loker', 'required|alpha_numeric');
		
			//if ($this->input->post('submit')) {
		if($this->form_validation->run()){
			
			$data['kode_kategori'] = $this->input->post('kode_kategori');
			$data['nama_kategori'] = $this->input->post('nama_kategori');

			$this->load->model('Materi');

			if($this->Materi->simpanKategori($data)) {
				
				$this->load->view('ctambahKategori');

			} else {
				echo "input data tidak berhasil";
			}

		} else {
			
			$this->load->view('ctambahKategori');
		}
		
	}

	public function editMateri(){
		$id = $this->uri->segment(3);
		
		$this->load->model('Materi');
		$data['kategori'] = $this->Materi->lihatKategori();
		$data['materi'] = $this->Materi->editMateri($id);	
		$this->load->view('editMateri', $data);	
	}

	public function updateMateri(){
		$data['id_materi'] = $this->input->post('id_materi');
		$data['kode_kategori'] = $this->input->post('kode_kategori');
		$data['judul'] = $this->input->post('judul');
		$data['isi'] = $this->input->post('isi');
		$this->load->model('Materi');
		if($this->Materi->updateMateri($data, $data['id_materi'])) {
			$this->index();
		} else {
			echo "input data tidak berhasil";

		}
	}

	public function ubah_profil(){
		$id = $this->session->userdata('username');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		//$this->load->view('ckepala', $data);
		
		$id = $this->session->userdata('username');
		$this->load->model('Tambah_User');
		$data['user_detail'] = $this->Tambah_User->user_detail($id);
		$this->load->view('ubah_profil', $data);
	}

	
}