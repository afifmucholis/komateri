<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tanya extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('forum');

		$this->load->view('tanya/header');

	}


	public function index() 
	{
		$data['lihat'] = $this->forum->lihat();
		$this->load->view('tanya/index', $data);
		$this->load->view('tanya/footer');
	}

	public function tambah()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required' );
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');

		if($this->form_validation->run()){
			$data['nama'] = $this->input->post('nama');
			$data['email'] = $this->input->post('email');
			$data['judul'] = $this->input->post('judul');
			$data['isi'] = $this->input->post('isi');

			
			if ($this->forum->tambah($data)) {
				redirect('tanya/index', 'refresh');
			}else{
				echo "gagal";
			}
		}else{
			$this->load->view('tanya/tanya');
			$this->load->view('tanya/footer');
		}
	}

	public function jawab()
	{
		$id = $this->uri->segment(3);
		$data['tanya'] = $this->forum->lihat1($id);
		$data['jawaban'] = $this->forum->jawaban($id);
		$this->load->view('tanya/jawab', $data);
		$this->load->view('tanya/footer');
		}

	public function menjawab(){
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');
		if($this->form_validation->run()){
			$data['id_tanya'] = $this->input->post('id_tanya');
			$data['nama'] = $this->input->post('nama');
			$data['isi'] = $this->input->post('isi');
			if ($this->forum->jawab($data)) {
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				echo "gagal";
			}
		}else{
			redirect($_SERVER['HTTP_REFERER']);
	}
}

}