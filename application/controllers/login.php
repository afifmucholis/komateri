<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
        parent::__construct();

        $this->load->model('Materi');
        $data['kategori'] = $this->Materi->lihatKategori();
        $this->load->view('header', $data);
        
    }

   

	public function index($page=NULL) 
	{
		$this->load->model('Materi');

	$config['base_url'] 		= base_url().'index.php/login/index';
	$config['total_rows'] 		= $this->Materi->daftar()->num_rows();
	$config['per_page'] 		= 4;
	$config['use_page_numbers'] = TRUE;
	$this->pagination->initialize($config);

	if($page > 0){
		$offset = $page * $config['per_page'] - $config['per_page'];
	}else{
		$offset = 0;
	}

		
		$dat['pagination'] = $this->pagination->create_links();
		$dat['materi'] = $this->Materi->daftar($config['per_page'],$offset)->result_array();
		$da['woe'] = $this->Materi->samping();

		

		if ($this->session->userdata('logged_in'))
		{
				
			redirect('user');
		}
		else
		{
		
			$this->load->view('login', $dat);
			$this->load->view('footer', $da);
		}
	}

	public function authenticate(){
		$da['woe'] = $this->Materi->samping();
		$this->form_validation->set_rules('username', 'Username');
		$this->form_validation->set_rules('password', 'Password');
		$data['kategori'] = $this->Materi->lihatKategori();

		if($this->form_validation->run()){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$this->load->model('auth');
			$check = $this->auth->check($username, $password);
			if ($check) {
				foreach ($check as $item) {
					# code...
				
				$data = array(
					'username' => $username,
					'level' => $item['level'],
					'logged_in' => TRUE);
				$this->session->set_userdata($data);
			}
				
				if ($this->session->userdata('level') == 0) {
					echo '<script language="javascript">';
					echo 'alert("$lvl")';
					echo '</script>';
					redirect('contributor/index');
				}else{
					redirect('user/index');
				}
				
			}
			else
			{

				echo '<script language="javascript">';
				echo 'alert("username atau password anda salah")';
				echo '</script>';
				
				$this->index();
				$this->load->view('footer', $da);
			}
		}else{
			 
			$this->index();
			$this->load->view('footer', $da);
		}
	}

	public function lihat(){
		$da['woe'] = $this->Materi->samping();
		$id = $this->uri->segment(3);
		$data['kateegori'] = $this->Materi->lihatKategori();

		$this->load->model('Materi');
		$data['lihat']=$this->Materi->lihat($id);

		
		$this->load->view('lihat', $data);
		$this->load->view('footer', $da);
		  
	}

	public function kate(){
		$da['woe'] = $this->Materi->samping();
		$id = $this->uri->segment(3);
		$data['kategori'] = $this->Materi->lihatKategori();

		$this->load->model('Materi');
		$data['lihat']=$this->Materi->kate($id);

		
		$this->load->view('kate', $data);
		$this->load->view('footer', $da);

	}

	public function cari(){
		
		$a = $this->input->post('cari');
		$this->load->model('Materi');
		$data['cari']=$this->Materi->cari($a);
		$data['kategori'] = $this->Materi->lihatKategori();
		
		
		$this->load->view('cari', $data);
		$this->load->view('footer');
		
	}

	public function profil(){
		$id = $this->uri->segment(3);
		$this->load->model('user');
		$data['profil'] = $this->user->profil($id);
		$this->load->view('profil', $data);
		$this->load->view('footer');
	}

}
	