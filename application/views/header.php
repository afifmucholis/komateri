<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->


        <title>KOMATERI </title>
        <link rel="icon" href="<?=base_url();?>assets/img/logo.png" type="image" />
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" />
        <!-- CORE JQUERY  -->
        <script src="<?=base_url();?>assets/js/jquery-1.11.1.js"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="<?=base_url();?>assets/js/bootstrap.js"></script>
        <!-- EASING SCROLL SCRIPTS PLUGIN  -->
        <script src="<?=base_url();?>assets/js/jquery.easing.min.js"></script>
        <!-- NICE SCROLL SCRIPTS PLUGIN  -->
        <script src="<?=base_url();?>assets/js/jquery.nicescroll.min.js"></script>
        <!-- CUSTOM SCRIPTS  -->
        <script src="<?=base_url();?>assets/js/custom.js"></script>

      </head>
      <body > 
       <div id="side-menu" class="move-me" >
        <hr class="hr-set" />
        <span class="logo-text" >KOMATERI   </span>  
        
        <strong> 
          <i class=" menu-close-icon fa fa-align-justify fa-2x" aria-hidden="true"></i> 
        </strong>


        <hr class="hr-set-two" />

        <ul  >
          <li>
            <?= anchor('login', "<i class='fa fa-home fa-2x'></i> Beranda")?>
          </li>
          <li>
           <!-- Split button -->
           <div class="btn-group">
            <a href="" data-toggle="dropdown">
              <button type="button" class="btn btn-default dropdown-toggle"  aria-expanded="false"> 
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button> Kategori </a>
              <ul class="dropdown-menu" role="menu">
               <?php 
               foreach($kategori as $item) {
                $nama = $item['nama_kategori'];
                $nama = word_limiter($nama, 2);
                echo "<li>".anchor('login/kate/'.$item['kode_kategori'], "$nama");
                echo "</li>";

              }
              ?>
            </ul>
          </div>
        </li>
        <li>
          <a href="#login" data-toggle="modal" data-target="#login"><i class="fa fa-lock fa-2x "></i>  Login</a>
        </li>
        <li>
          <div class="col-lg-8">
            <?php echo form_open('login/cari');?>
            <div class="input-group custom-search-form">
              <div class="input-control text">
                <span class="input-group-btn">
                <input type="text" class="form-control" placeholder="Cari Judul Materi" name="cari">
                  <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </div>
            </form>
              </div>   
            </div>          
            </li>
          </ul>
          <div class="intro-txt">
           <br />
           <i><strong>KOMATERI</strong></i>
           <hr />
           <p>
             Komateri adalah website yang dibangun untuk share materi kuliah anak-anak Komputer dan Sistem Informasi, Sekolah Vokasi, Universitas Gadjah Mada. Jika yang anda cari tidak ada, ataupun ingin bertanya seputar materi kuliah silahkan kunjungi <?=anchor('tanya/index',"<button class='btn btn-info'>FORUM</button>")?>
           </p>

         </div>



       </div>
       <!-- SIDE MENU SECTION END-->  
       

       <div class="social-menu" >
        <i class="fa fa-angle-double-right fa-2x close-social"></i>
       
        <a href="https://www.facebook.com/sharer/sharer.php?u=URLENCODED_URL&t=TITLE"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"target="_blank" title="Share on Facebook">
        <i class="fa fa-facebook fa-2x social-icon-set"></i>
      </a>

        <a href="https://twitter.com/share?url=URLENCODED_URL&via=TWITTER_HANDLE&text=TEXT"
   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
   target="_blank" title="Share on Twitter">
<i class="fa fa-twitter fa-2x social-icon-set"></i></a> 
       <a href="https://plus.google.com/share?url=URLENCODED_URL"
   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
   target="_blank" title="Share on Google+">
<i class="fa fa-google-plus fa-2x social-icon-set"></i></a> 
      </div> 

      <!-- SOCIAL MENU SECTION END-->  

      <section id="home-sec" >
       <div class="overlay">

         <strong> <i class="menu-open-icon fa fa-align-justify fa-3x"></i> </strong>
         <i class="menu-open-social fa fa-share-square-o  fa-2x pull-right"> share</i> 

         <div class="container">
           <div class="row p-top">
             <div class="col-lg-4 col-md-4 col-sm-4">
              <h1 align="center"><i class='fa fa-book fa-2x'></i><br>KOMATERI</h1>

            </div>
            <div class="col-lg-8 col-md-8 col-sm-8" >


              <!--Login -->
              <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">

                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <h4 class="modal-title text-primary" id="myModalLabel">Login</h4>
                    </div>
                    <div class="modal-body">
                     <div class="form-group">
                      <?php echo form_open('login/authenticate');?>
                      <div class="form-group">
                        <label for="username-email" class="text-primary">Username</label>
                        <input name='username' id="username-email" placeholder="Username" type="text" class="form-control" />
                      </div>
                      <div class="form-group">
                        <label for="password" class="text-primary">Password</label>
                        <input type="password" id="password" name='password' placeholder="Password" type="text" class="form-control" />
                      </div>
                      <div class="form-group text-center">
                        <input type="submit" class="btn btn-info btn-login-submit" value="Login" />
                      </div>
                      <?php echo form_close(); ?>
                      
                    </div>
                    <div class="modal-footer">

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- login END-->  