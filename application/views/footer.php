</div>

     </section>
     <!-- HOME SECTION END-->
     <section id="features-sec"  >
      <div class="container"  >
       <div class="row">

         <p class="footer-p">
           &copy; 2014 komateri | Design by <a href="http://www.binarytheme.com/" target="_blank" >binarytheme.com</a> edit by komateri
         </p>
       </div>

     </div>

   </div>
 </div>
</section>
<!-- CONTACT SECTION END-->
<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
<!-- CORE JQUERY  -->
<script src="assets/js/jquery-1.11.1.js"></script>
<!-- BOOTSTRAP SCRIPTS  -->
<script src="assets/js/bootstrap.js"></script>
<!-- EASING SCROLL SCRIPTS PLUGIN  -->
<script src="assets/js/jquery.easing.min.js"></script>
<!-- NICE SCROLL SCRIPTS PLUGIN  -->
<script src="assets/js/jquery.nicescroll.min.js"></script>
<!-- CUSTOM SCRIPTS  -->
<script src="assets/js/custom.js"></script>
</body>
</html>
