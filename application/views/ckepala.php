
<?php
foreach($user_detail as $item) {

 ?>

 <!DOCTYPE html>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>KOMATERI</title>
  <link rel="icon" href="<?=base_url();?>assets/img/logo.png" type="image" />
  <!--untuk upload-->
  <script src="<?php echo base_url()?>assets/js/site.js"></script>
  <script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
  <!-- BOOTSTRAP STYLES-->
  <script type="text/javascript" src="<?=base_url();?>ckeditor/ckeditor.js"></script>
  <link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
  <!--CUSTOM BASIC STYLES-->
  <link href="<?=base_url();?>assets/css/basic.css" rel="stylesheet" />
  <!--CUSTOM MAIN STYLES-->
  <link href="<?=base_url();?>assets/css/custom.css" rel="stylesheet" />
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <span class="navbar-brand">KOMATERI</span>
      </div>

      <div class="header-right">
        <?php echo anchor('contributor/lihatMateri', "<span class='btn btn-warning'><b>Materi </b><i class='fa fa-list fa-2x'></i></span>"); ?>
        <?php echo anchor('contributor/tambahMateri', " <span class='btn btn-warning' ><b>+ Materi</b><i class='fa fa-pencil fa-2x'></i></span>"); ?>              
        <?php echo anchor('contributor/lihatKategori', "<span class='btn btn-primary'><b>Kategori </b><i class='fa fa-th-list fa-2x'></i></span>"); ?>
        <?php echo anchor('contributor/tambahKategori', " <span class='btn btn-primary' ><b>+ Kategori</b><i class='fa fa-pencil-square-o fa-2x'></i></span>"); ?>              
        <?php echo anchor('contributor/logout', "<span class='btn btn-danger'><b>Logout</b><i class='fa fa-power-off fa-2x'></i></span>");?>


      </div>
    </nav>

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li>
            <div class="user-img-div">
              <img src="<?=base_url()?>uploads/<?=$item['foto']?>" class="img-thumbnail" /> <?=$item['username']?>

              <div class="inner-text">


                <div align="left">
                  <br>Nama : <br>
                  <div align="right"><?=$item['nama']?> <br></div>
                  Jenis kelamin : <br>
                  <?php if ($item['jk'] == 'L') {
                    echo "<div align='right'>Laki-laki <br> </div>";
                  }else{
                    echo "<div align='right'>Perempuan <br> </div>";
                  }?>
                  E-mail : <br>
                  <div align="right"> <?=$item['email']?>  <br> </div>
                  Nomor Telpon : <br>
                  <div align="right"> <?=$item['telpon']?> <br> </div>
                  Kota Tinggal : <br>
                  <div align="right"> <?=$item['alamat']?> <br> </div>
                  Deskripsi : <br>
                  <div align="right"> <?=$item['deskripsi']?> <br> </div>
                  <?=anchor('contributor/ubah_profil',"<button class='btn btn-info'> Ubah Profil</button>")?>
                </div>

              </li>


            </ul>
          </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
          <div id="page-inner">
            <div class="row">
              <div class="col-md-12">

                <?php
              }
              ?>