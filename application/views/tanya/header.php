<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>KOMATERI</title>
    <link rel="icon" href="<?=base_url();?>assets/img/logo.png" type="image" />
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <!-- Fontawesome core CSS -->
    <link href="<?=base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Timeline -->
    <link href="<?=base_url();?>assets/css/timeline.css" rel="stylesheet" />
    <!-- custom CSS here -->
    <link href="<?=base_url();?>assets/css/stylee.css" rel="stylesheet" />
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?=anchor('tanya/index', "<span class='navbar-brand'>FORUM</span>")?>
            </div>
            <!-- Collect the nav links for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><?=anchor('login/index', 'Baca Materi')?></li>
                    <li><?=anchor('tanya/tambah', 'Tanya')?></li>
                    <li><?=anchor('tanya/', 'Daftar Pertanyaan')?></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <div class="container" >
        <div class="row ">            
