<?php
foreach ($tanya as $key) {

  ?>
  <div class="col-md-offset-2 col-md-8 col-sm-12 top-margin" >
    <div >
      <ul class="timeline">
        <li class="time-label">
          <span class="bg-orange">
          </span>
          <br />
          <br />
        </li>
        <li class="time-label">
          <span class="bg-light-blue"> Daftar Pertanyaan
          </span>

        </li>
        <li>
          <i class='fa fa-envelope bg-blue'></i>
          <div class='timeline-item'>
            <span class='time'><i class='fa fa-clock-o'></i><?=$key['time']?></span>
            <h3 class='timeline-header'><a href='#'><?=$key['judul']?></a> <?=$key['nama']?></h3>
            <div class='timeline-body'>
             <?=$key['isi']?>

             <div class='timeline-footer'>



              <div class="well">
                <?=form_open('tanya/menjawab')?>
                <h3 class='timeline-header'>
                  <input type="text" class="form-control" name="nama" placeholder="Nama">
                  <input type="hidden" name="id_tanya" value="<?=$key['id_tanya']?>">
                </h3>
                <div class='timeline-body'>
                 <textarea class="form-control" name="isi"></textarea>

                 <?php echo form_error('nama'); ?>
                 <?php echo form_error('isi'); ?>

               </div>

               <div class='timeline-footer'>
                <input type="submit" class='btn btn-primary' value="Jawab">
                </div>
              </form>
            
            <?php
            foreach ($jawaban as $key) { ?>
            <strong class='timeline-header'> <?=$key['nama']?></strong> 
            <div class='timeline-body'>
             <?=$key['isi']?>
             <p align="right"><i class='fa fa-clock-o'></i><?=$key['time']?> </p>
           </div>
            <?php } ?>
          </div>

        </div>

      </div>

    </div>

  </div>
</li>


<?php
}
?>