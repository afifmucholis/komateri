            <div class="col-md-offset-2 col-md-8 col-sm-12 top-margin" >
                <div >
                    <ul class="timeline">
                        <li class="time-label">
                            <span class="bg-orange">
                            </span>
                            <br />
                            <br />
                        </li>

                        <li class="time-label">
                            <span class="bg-light-blue"> Form Pertanyaan
                            </span>
                        </li>

                        <li>
                            <i class='fa fa-pencil bg-blue'></i>
                            <div class='timeline-item'>

                                <h3 class='timeline-header'>
                                    <?=form_open('tanya/tambah')?>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                               <input type="text" name="nama"  placeholder="Nama" class="form-control">
                                           </td>
                                           <td>
                                               <input type="email" name="email"   placeholder="Email" class="form-control">
                                           </td>   
                                       </tr>
                                   </table>

                               </h3>
                               <h3 class='timeline-header'>
                                <input type="text" name="judul" class="form-control" id="exampleInputEmail1" placeholder="Judul">
                            </h3>
                            <div class='timeline-body'>
                             <textarea class="form-control" name="isi">

                             </textarea>
                         </div>

                         <div class='timeline-footer'>
                            <input type="submit" class='btn btn-primary' value="Tanya">
                            <?php echo form_error('nama'); ?>
                            <?php echo form_error('email'); ?>
                            <?php echo form_error('judul'); ?>
                            <?php echo form_error('isi'); ?>
                        </div>
                    </div>
                    </form>
                </li>
