<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku_tamu extends CI_Model {

public function simpan($data) {
	if ($this->db->insert('buku_tamu', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

 public function lihat() {
 		$query = $this->db->get('buku_tamu');
 		if ($query->num_rows() > 0) {
 			return $query->result_array();
			
 		} else {
 			return FALSE;
 		}
 }

 public function hapus($id)
  {

      $this->db->where('id', $id);
      $this->db->delete('buku_tamu'); 
  
}
}
?>