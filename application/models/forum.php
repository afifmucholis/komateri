<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forum extends CI_Model {

	public function tambah($data)
	{
		if ($this->db->insert('tanya', $data)) {
			return true;
		}else{
			return false;
		}
	}

	public function lihat()
	{
		$this->db->order_by("time", "desc"); 
		$query = $this->db->get('tanya');
		return $query->result_array();
	}

	public function jawab($data)
	{
		if ($this->db->insert('jawab', $data)) {
			return true;
		}else{
			return false;
		}
	}

	public function lihat1($id)
	{
		$this->db->where("id_tanya", $id); 
		$query = $this->db->get('tanya');
		return $query->result_array();
	}

	public function jawaban($id)
	{
		$this->db->where("id_tanya", $id); 
		$this->db->order_by("time", "desc");
		$query = $this->db->get('jawab');
		return $query->result_array();
	}
}
?>