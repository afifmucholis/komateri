<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tambah_User extends CI_Model {

public function simpan($data) {
	if ($this->db->insert('user', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

 public function daftar() {
 		$query = $this->db->get('user_detail');
 		if ($query->num_rows() > 0) {
 			return $query->result_array();
			
 		} else {
 			return FALSE;
 		}
 }

 public function hapus($id)
  {

      $this->db->where('username', $id);
      $this->db->delete('user_detail'); 
  
}
public function user_detail($id) {
		$query = $this->db->get_where('user_detail', array('username' => $id)); 

 			return $query->result_array();
 }
}
?>