<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materi extends CI_Model {

public function simpan($data) {
	if ($this->db->insert('materi', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

 public function daftar($limit=NULL, $offset=NULL) {
 		if($limit != NULL){
 			
			$this->db->limit($limit, $offset);
		} 
 			$this->db->select("*");
 			$this->db->from("materi");  
 			$this->db->order_by("id_materi", "desc"); 
			return $this->db->get('');
 		
 }

 public function daftarlagi() {
 		
 			$this->db->select("*");
 			$this->db->from("materi");  
 			$this->db->order_by("id_materi", "desc"); 
			$query = $this->db->get('');
			return $query->result_array();
 		
 }

 public function simpanContent($data) {
	if ($this->db->insert('content', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

public function lihatContent() {
 		$query = $this->db->get('content');
 		
 			return $query->result_array();
			
 		
 }

 public function lihatKategori() {
 		$query = $this->db->get('kategori');
 		if ($query->num_rows() > 0) {
 			return $query->result_array();
			
 		} else {
 			return FALSE;
 		}
 }

 public function simpanKategori($data) {
	if ($this->db->insert('kategori', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

public function hapusKategori($id)
  {

      $this->db->where('kode_kategori', $id);
      $this->db->delete('kategori'); 
  }

 public function hapusmateri($id)
  {

      $this->db->where('id_materi', $id);
      $this->db->delete('materi'); 
  }



public function samping() {
 		$query = $this->db->query("select * from materi order by id_materi desc limit 5");

		if ($query->num_rows() > 0) {
 			return $query->result_array();
			
 		} else {
 			return FALSE;
 		}
 }

 public function lihat($id){
 	$query = $this->db->get_where('materi', array('id_materi' => $id));
 	return $query->result_array();
 }

 public function kate($id){
 	$query = $this->db->get_where('materi', array('kode_kategori' => $id));
 	return $query->result_array();
 }

 public function cari($a){
	//$sql = "SELECT * FROM materi WHERE judul LIKE '%".$a."%'";
	$this->db->select('*');
	$this->db->from('materi');  
	$this->db->like('judul', $a); 
	$query = $this->db->get(); 
	
	return $query->result_array();
 }

 public function clihatMateri($username){
 		$this->db->select("*");
 		$this->db->from("materi");  
 		$this->db->order_by("id_materi", "desc"); 
 		$this->db->where('username', $username); 
 		$query = $this->db->get();
 		return $query->result_array();
 	}

 public function editMateri($id){
 		$this->db->select("*");
 		$this->db->from("materi");  
 		$this->db->order_by("id_materi", "desc"); 
 		$this->db->where('id_materi', $id); 
 		$query = $this->db->get();
 		if ($query->num_rows() > 0) {
 			return $query->result_array();
			
 		} else {
 			return FALSE;
 		}

 }

public function editKategori($id){
 		$this->db->select("*");
 		$this->db->from("kategori"); 
 		$this->db->where('kode_kategori', $id); 
 		$query = $this->db->get();
 		return $query->result_array();
 }

 public function updateMateri($data){
 	$id = $data['id_materi'];
 	if ($this->db->update('materi', $data, array('id_materi' => $id))) {
 		# code...
 		return TRUE;
 	}else{
 		return FALSE;
 	}
 }

 public function updateKategori($data, $id){

 	if ($this->db->update('kategori', $data, array('kode_kategori' => $id))) {
 		return TRUE;
 	}else{
 		return FALSE;
 	}
 }
}
?>