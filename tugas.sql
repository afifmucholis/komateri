-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2014 at 02:09 AM
-- Server version: 5.6.16-log
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tugas`
--

-- --------------------------------------------------------

--
-- Table structure for table `jawab`
--

CREATE TABLE IF NOT EXISTS `jawab` (
  `id_jawab` int(11) NOT NULL AUTO_INCREMENT,
  `id_tanya` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `isi` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jawab`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `jawab`
--

INSERT INTO `jawab` (`id_jawab`, `id_tanya`, `nama`, `isi`, `time`) VALUES
(1, 4, 'afif', 'ini lagi dibuat sabar ya', '2014-12-23 17:36:41'),
(2, 4, 'afif', 'sabar bu dikit lagi jadi', '2014-12-23 17:37:19'),
(3, 5, 'x', 'apapa', '2014-12-23 18:16:10'),
(4, 5, 'xx', 'ini kyaknya sudah bisa', '2014-12-23 18:16:38'),
(5, 6, 'Mrs.X', 'Amin', '2014-12-23 18:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `kode_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kode_kategori`, `nama_kategori`) VALUES
('Basdat', 'Basis Data'),
('KWN', 'Kewarganegaraan'),
('RPL', 'Rekayasa Perangkat Lunak'),
('Web', 'Pemrograman Web Lanjut');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE IF NOT EXISTS `materi` (
  `id_materi` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `kode_kategori` varchar(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_materi`),
  KEY `fku` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `username`, `kode_kategori`, `judul`, `isi`, `tanggal`) VALUES
(2, 'q', 'Basdat', 'Transaction', '0', '2014-12-21 10:12:38'),
(10, 'a', 'Basdat', 'Basis Data 2,7', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>\r\n', '2014-12-23 23:40:40'),
(11, 'q', 'RPL', 'DAD', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry''s standard dummy text.', '2014-12-20 12:09:08'),
(12, 'q', 'Web', 'HTNL', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry''s standard dummy text.', '2014-12-20 12:09:37'),
(16, 'q', 'Basdat', 'Basis Data Lanjutansa', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry''s standard dummy text.', '2014-12-20 12:09:46'),
(18, 'a', 'Basdat', 'Basis Data 1,5', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>\r\n', '2014-12-23 23:39:27'),
(19, 'q', 'Basdat', 'Apa ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry''s standard dummy text.', '2014-12-20 12:11:20'),
(22, 'q', 'Web', 'CI', '<p><img alt="" src="/tugas/kcfinder/upload/images/0.jpg" style="height:250px; width:200px" />disini isinya terserah Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry''s standard dummy text.</p>\n', '2014-12-20 12:11:58'),
(23, 'q', 'RPL', 'ini judul', '<p>0dfgcjknl</p>\r\n', '2014-12-21 11:32:06'),
(24, 'a', 'KWN', 'APA', '<p>disini isinya <img alt="" src="/tugas/kcfinder/upload/images/foto_0000001520130612133705.jpg" style="height:960px; width:829px" /></p>\r\n', '2014-12-23 13:14:05'),
(25, 'a', 'Web', 'PHP', '<p style="text-align:start"><strong>PHP</strong> is a server-side scripting language designed for web development but also used as a general-purpose programming language. As of January 2013, PHP was installed on more than 240 million websites (39% of those sampled) and 2.1 million web servers.Originally created by Rasmus Lerdorf in 1994, the reference implementation of PHP (powered by the <a href="http://en.wikipedia.org/wiki/Zend_Engine" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Zend Engine">Zend Engine</a>) is now produced by The PHP Group.<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-about_PHP-6" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[6]</a></sup> While PHP originally stood for <em>Personal Home Page</em>,<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-5" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[5]</a></sup> it now stands for <em>PHP: Hypertext Preprocessor</em>, which is a <a href="http://en.wikipedia.org/wiki/Recursive_acronym" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Recursive acronym">recursive</a> <a href="http://en.wikipedia.org/wiki/Backronym" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Backronym">backronym</a>.<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-7" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[7]</a></sup></p>\r\n\r\n<p style="text-align:start">PHP code can be simply mixed with <a href="http://en.wikipedia.org/wiki/HTML" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="HTML">HTML</a> code, or it can be used in combination with various <a href="http://en.wikipedia.org/wiki/Web_template_system" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Web template system">templating engines</a> and <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Web_framework" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Web framework">web frameworks</a>. PHP code is usually processed by a PHP <a href="http://en.wikipedia.org/wiki/Interpreter_(computing)" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Interpreter (computing)">interpreter</a>, which is usually implemented as a web server&#39;s native <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Plugin_(computing)" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Plugin (computing)">module</a> or a <a href="http://en.wikipedia.org/wiki/Common_Gateway_Interface" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Common Gateway Interface">Common Gateway Interface</a> (CGI) executable. After the PHP code is interpreted and executed, the web server sends resulting output to its client, usually in form of a part of the generated web page&nbsp;&ndash; for example, PHP code can generate a web page&#39;s HTML code, an image, or some other data. PHP has also evolved to include a <a href="http://en.wikipedia.org/wiki/Command-line_interface" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Command-line interface">command-line interface</a> (CLI) capability and can be used in<a class="mw-redirect" href="http://en.wikipedia.org/wiki/Computer_software" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Computer software">standalone</a> <a href="http://en.wikipedia.org/wiki/Graphical_user_interface" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Graphical user interface">graphical applications</a>.<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-8" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[8]</a></sup></p>\r\n\r\n<p style="text-align:start">The canonical PHP interpreter, powered by the Zend Engine, is <a href="http://en.wikipedia.org/wiki/Free_software" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Free software">free software</a> released under the <a href="http://en.wikipedia.org/wiki/PHP_License" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="PHP License">PHP License</a>. PHP has been widely ported and can be deployed on most web servers on almost every <a href="http://en.wikipedia.org/wiki/Operating_system" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Operating system">operating system</a> and <a href="http://en.wikipedia.org/wiki/Computing_platform" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Computing platform">platform</a>, free of charge.<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-foundations-9" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[9]</a></sup></p>\r\n\r\n<p style="text-align:start">Despite its popularity, no written <a href="http://en.wikipedia.org/wiki/Formal_specification" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Formal specification">specification</a> or standard existed for the PHP language until 2014, leaving the canonical PHP interpreter as a <em><a href="http://en.wikipedia.org/wiki/De_facto" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="De facto">de facto</a></em> standard. Since 2014, there is ongoing work on creating a formal PHP specification.<sup><a href="http://en.wikipedia.org/wiki/PHP#cite_note-10" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[10]</a></sup></p>\r\n', '2014-12-24 00:14:55'),
(26, 'a', 'KWN', 'Pancasila', '<p style="text-align:start"><strong>Pancasila</strong> adalah ideologi dasar bagi negara Indonesia. Nama ini terdiri dari dua kata dari Sanskerta: <em>pa&ntilde;ca</em> berarti lima dan <em>??la</em> berarti prinsip atau asas. Pancasila merupakan rumusan dan pedoman kehidupan berbangsa dan bernegara bagi seluruh rakyat Indonesia.</p>\r\n\r\n<p style="text-align:start">Lima sendi utama penyusun Pancasila adalah Ketuhanan Yang Maha Esa, kemanusiaan yang adil dan beradab, persatuan Indonesia, kerakyatan yang dipimpin oleh hikmat kebijaksanaan dalam permusyawaratan/perwakilan, dan keadilan sosial bagi seluruh rakyat Indonesia, dan tercantum pada paragraf ke-4 Preambule (Pembukaan) Undang-undang Dasar 1945.</p>\r\n\r\n<p style="text-align:start">Meskipun terjadi perubahan kandungan dan urutan lima sila Pancasila yang berlangsung dalam beberapa tahap selama masa perumusan Pancasilapada tahun 1945, tanggal 1 Juni diperingati sebagai hari lahirnya Pancasila.</p>\r\n\r\n<p style="text-align:start">Dalam upaya merumuskan Pancasila sebagai dasar negara yang resmi, terdapat usulan-usulan pribadi yang dikemukakan dalam Badan Penyelidik Usaha Persiapan Kemerdekaan Indonesia yaitu:</p>\r\n\r\n<ul style="list-style-type:disc">\r\n	<li><em>Lima Dasar</em> oleh <a class="mw-redirect" href="http://id.wikipedia.org/wiki/Muhammad_Yamin" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Muhammad Yamin">Muhammad Yamin</a>, yang berpidato pada tanggal <a href="http://id.wikipedia.org/wiki/29_Mei" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="29 Mei">29 Mei</a> <a href="http://id.wikipedia.org/wiki/1945" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1945">1945</a>. <a class="mw-redirect" href="http://id.wikipedia.org/wiki/Muhammad_Yamin" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Muhammad Yamin">Yamin</a> merumuskan lima dasar sebagai berikut: Peri Kebangsaan, Peri Kemanusiaan, Peri Ketuhanan, Peri Kerakyatan, dan Kesejahteraan Rakyat. Dia menyatakan bahwa kelima sila yang dirumuskan itu berakar pada sejarah, peradaban, agama, dan hidup ketatanegaraan yang telah lama berkembang di <a href="http://id.wikipedia.org/wiki/Indonesia" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Indonesia">Indonesia</a>.<a href="http://id.wikipedia.org/wiki/Mohammad_Hatta" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Mohammad Hatta">Mohammad Hatta</a> dalam memoarnya meragukan pidato Yamin tersebut.<sup><a href="http://id.wikipedia.org/wiki/Pancasila#cite_note-Suwarno_p12-1" style="text-decoration: none; color: rgb(11, 0, 128); background: none;">[1]</a></sup></li>\r\n	<li><em>Panca Sila</em> oleh <a href="http://id.wikipedia.org/wiki/Soekarno" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Soekarno">Soekarno</a> yang dikemukakan pada tanggal <a href="http://id.wikipedia.org/wiki/1_Juni" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1 Juni">1 Juni</a> <a href="http://id.wikipedia.org/wiki/1945" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1945">1945</a> dalam pidato spontannya yang kemudian dikenal dengan judul &quot;<em><a href="http://id.wikipedia.org/wiki/Lahirnya_Pancasila" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Lahirnya Pancasila">Lahirnya Pancasila</a></em>&quot;. Sukarno mengemukakan dasar-dasar sebagai berikut: Kebangsaan; Internasionalisme; Mufakat, dasar perwakilan, dasar permusyawaratan; Kesejahteraan; Ketuhanan. Nama Pancasila itu diucapkan oleh Soekarno dalam pidatonya pada tanggal 1 Juni itu, katanya:</li>\r\n</ul>\r\n\r\n<p><em>Sekarang banyaknya prinsip: kebangsaan, internasionalisme, mufakat, kesejahteraan, dan ketuhanan, lima bilangannya. Namanya bukan Panca Dharma, tetapi saya namakan ini dengan petunjuk seorang teman kita ahli bahasa - namanya ialah Pancasila. Sila artinya azas atau dasar, dan diatas kelima dasar itulah kita mendirikan negara Indonesia, kekal dan abadi.</em></p>\r\n\r\n<p style="text-align:start">Setelah Rumusan Pancasila diterima sebagai dasar negara secara resmi beberapa dokumen penetapannya ialah:</p>\r\n\r\n<ul style="list-style-type:disc">\r\n	<li>Rumusan Pertama: <a href="http://id.wikipedia.org/wiki/Piagam_Jakarta" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Piagam Jakarta">Piagam Jakarta</a> (Jakarta Charter) - tanggal <a href="http://id.wikipedia.org/wiki/22_Juni" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="22 Juni">22 Juni</a> <a href="http://id.wikipedia.org/wiki/1945" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1945">1945</a></li>\r\n	<li>Rumusan Kedua: Pembukaan Undang-undang Dasar - tanggal <a href="http://id.wikipedia.org/wiki/18_Agustus" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="18 Agustus">18 Agustus</a> <a href="http://id.wikipedia.org/wiki/1945" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1945">1945</a></li>\r\n	<li>Rumusan Ketiga: Mukaddimah Konstitusi Republik Indonesia Serikat - tanggal <a href="http://id.wikipedia.org/wiki/27_Desember" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="27 Desember">27 Desember</a> <a href="http://id.wikipedia.org/wiki/1949" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1949">1949</a></li>\r\n	<li>Rumusan Keempat: Mukaddimah Undang-undang Dasar Sementara - tanggal <a href="http://id.wikipedia.org/wiki/15_Agustus" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="15 Agustus">15 Agustus</a> <a href="http://id.wikipedia.org/wiki/1950" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="1950">1950</a></li>\r\n	<li>Rumusan Kelima: Rumusan Kedua yang dijiwai oleh Rumusan Pertama (merujuk <a class="mw-redirect" href="http://id.wikipedia.org/wiki/Dekrit_presiden" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Dekrit presiden">Dekrit Presiden 5 Juli 1959</a></li>\r\n</ul>\r\n\r\n<p>dari wikipedia</p>\r\n', '2014-12-24 00:16:21'),
(27, 'a', 'RPL', 'Sejarah RPL', '<p style="text-align:start">When the first digital <a href="http://en.wikipedia.org/wiki/Computer" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Computer">computers</a> appeared in the early 1940s,<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-4" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[4]</a></sup> the instructions to make them operate were wired into the machine. Practitioners quickly realized that this design was not flexible and came up with the &quot;stored program architecture&quot; or <a href="http://en.wikipedia.org/wiki/Von_Neumann_architecture" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Von Neumann architecture">von Neumann architecture</a>. Thus the division between &quot;hardware&quot; and &quot;software&quot; began with <a href="http://en.wikipedia.org/wiki/Abstraction_(computer_science)" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Abstraction (computer science)">abstraction</a>being used to deal with the complexity of computing.</p>\r\n\r\n<p style="text-align:start"><a href="http://en.wikipedia.org/wiki/Programming_language" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Programming language">Programming languages</a> started to appear in the 1950s and this was also another major step in abstraction. Major languages such as <a href="http://en.wikipedia.org/wiki/Fortran" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Fortran">Fortran</a>, <a href="http://en.wikipedia.org/wiki/ALGOL" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="ALGOL">ALGOL</a>, and <a href="http://en.wikipedia.org/wiki/COBOL" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="COBOL">COBOL</a> were released in the late 1950s to deal with scientific, algorithmic, and business problems respectively. <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Edsger_Dijkstra" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Edsger Dijkstra">E.W. Dijkstra</a> wrote his seminal paper, &quot;Go To Statement Considered Harmful&quot;,<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-5" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[5]</a></sup> in 1968 and <a href="http://en.wikipedia.org/wiki/David_Parnas" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="David Parnas">David Parnas</a> introduced the key concept of <a href="http://en.wikipedia.org/wiki/Modularity" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Modularity">modularity</a> and <a href="http://en.wikipedia.org/wiki/Information_hiding" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Information hiding">information hiding</a> in 1972<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-6" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[6]</a></sup> to help programmers deal with the ever increasing complexity of<a class="mw-redirect" href="http://en.wikipedia.org/wiki/Software_systems" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Software systems">software systems</a>.</p>\r\n\r\n<p style="text-align:start">The term &quot;software engineering&quot; was first used in 1968 as a title for the world&#39;s first conference on software engineering, sponsored and facilitated by NATO. The conference was attended by international experts on software who agreed on defining best practices for software grounded in the application of engineering. The result of the conference is a report that defines how software should be developed [i.e., software engineering foundations]. The original report is publicly available.<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-7" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[7]</a></sup></p>\r\n\r\n<p style="text-align:start">The discipline of software engineering was created to address poor quality of software, get projects exceeding time and budget under control, and ensure that software is built systematically, rigorously, measurably, on time, on budget, and within specification. Engineering already addresses all these issues, hence the same principles used in engineering can be applied to software. The widespread lack of best practices for software at the time was perceived as a &quot;<a href="http://en.wikipedia.org/wiki/Software_crisis" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Software crisis">software crisis</a>&quot;.<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-8" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[8]</a></sup><sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-9" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[9]</a></sup><sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-10" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[10]</a></sup></p>\r\n\r\n<p style="text-align:start"><a href="http://en.wikipedia.org/wiki/Barry_Boehm" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Barry Boehm">Barry W. Boehm</a> documented several key advances to the field in his 1981 book, &#39;Software Engineering Economics&#39;.<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-11" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[11]</a></sup> These include his Constructive Cost Model (<a class="mw-redirect" href="http://en.wikipedia.org/wiki/COCOMO_II" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="COCOMO II">COCOMO</a>), which relates software development effort for a program, in man-years T, to <em>source lines of code</em> (SLOC). <img alt=" T = k * (SLOC)^{(1+x)}" class="mwe-math-fallback-image-inline tex" src="http://upload.wikimedia.org/math/b/4/e/b4ed9dfed53d289b17c3808dac6ca42b.png" style="border:none; display:inline-block; margin:0px; vertical-align:middle" /></p>\r\n\r\n<p style="text-align:start">The book analyzes sixty-three software projects and concludes the cost of fixing errors escalates as we move the project toward field use. The book also asserts that the key driver of software cost is the capability of the software development team.</p>\r\n\r\n<p style="text-align:start">In 1984, the <a href="http://en.wikipedia.org/wiki/Software_Engineering_Institute" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Software Engineering Institute">Software Engineering Institute</a> (SEI) was established as a federally funded research and development center headquartered on the campus of Carnegie Mellon University in Pittsburgh, Pennsylvania, United States. <a href="http://en.wikipedia.org/wiki/Watts_Humphrey" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Watts Humphrey">Watts Humphrey</a> founded the SEI Software Process Program, aimed at understanding and managing the software engineering process. His 1989 book, Managing the Software Process,<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-12" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[12]</a></sup> asserts that the Software Development Process can and should be controlled, measured, and improved. The Process Maturity Levels introduced would become the Capability Maturity Model Integration for Development(CMMi-DEV), which has defined how the US Government evaluates the abilities of a software development team.</p>\r\n\r\n<p style="text-align:start">Modern, generally accepted best-practices for software engineering have been collected by the <a href="http://en.wikipedia.org/wiki/ISO/IEC_JTC_1/SC_7" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="ISO/IEC JTC 1/SC 7">ISO/IEC JTC 1/SC 7</a> subcommittee and published as the <a href="http://en.wikipedia.org/wiki/Software_Engineering_Body_of_Knowledge" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Software Engineering Body of Knowledge">Software Engineering Body of Knowledge</a> (SWEBOK).<sup><a href="http://en.wikipedia.org/wiki/Software_engineering#cite_note-13" style="text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background: none;">[13]</a></sup></p>\r\n\r\n<p style="text-align:start"><sup>sumber : wikipedia</sup></p>\r\n', '2014-12-24 00:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `tanya`
--

CREATE TABLE IF NOT EXISTS `tanya` (
  `id_tanya` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tanya`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tanya`
--

INSERT INTO `tanya` (`id_tanya`, `nama`, `email`, `judul`, `isi`, `time`) VALUES
(1, 'afif', 'afifmucholis@gmail.com', 'tanya apa?', 'ini sudah bisa apa belom?\r\n                             ', '2014-12-23 16:13:58'),
(2, 'mucholis', 'mucholis@yah.oo', 'ini coba', 'ini sudah bisa dicoba belum?', '2014-12-23 16:16:41'),
(3, 'afif', 'afifmucholis@gmail.com', 'tanya apa?', 'ini sudah bisa apa belom?\r\n                             ', '2014-12-23 16:25:35'),
(4, 'SAYA', 'saya@a.com', 'Tanya lagi', 'ini jawabnya udah bisa apa belom?\r\n\r\n                             ', '2014-12-23 16:54:25'),
(5, 'kami', 'ka@mi', 'redirect', 'ini redirectnya udah bisa belom ya??\r\n                             ', '2014-12-23 17:00:34'),
(6, 'Mr.X', 'x@xxx.x', 'Akhirnya', 'akhirnya selesai juga, semoga bugnya tidak ketahuan\r\n                             ', '2014-12-23 18:22:52');

-- --------------------------------------------------------

--
-- Stand-in structure for view `user`
--
CREATE TABLE IF NOT EXISTS `user` (
`username` varchar(20)
,`password` varchar(32)
,`level` int(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE IF NOT EXISTS `user_detail` (
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `level` int(10) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `telpon` varchar(20) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(100) NOT NULL DEFAULT 'no.jpg',
  `alamat` text NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`username`, `password`, `nama`, `email`, `level`, `jk`, `telpon`, `deskripsi`, `foto`, `alamat`) VALUES
('a', '0cc175b9c0f1b6a831c399e269772661', 'afif mucholis', 'afif.mucholis@gmail.com', 1, 'L', '0987654143', ' ini deskripsi saya, daripada tidak diisi ', '3328982534a016865e9e2fc281de46d1.jpg', 'bacok'),
('kampret', 'cfdefa9979cce5a1e1b5c577b429360c', 'q', 'q', 0, 'L', 'q', 'qsdas ads        ', 'b799c0bfbd0d867b8e8630299f21cc7d.jpg', 'q'),
('q', '7694f4a66316e53c8cdd9d9954bd611d', 'Contributor WOW', 'contributor@ggmild.com', 0, 'L', '08979789098788', 'saya sebagai contributor di web ini dan saya tidak tahu mau nulis apa', '01905cef1cff513ae8e170d5406885db.jpg', 'kubur'),
('r', '71b0438bf46aa26928c7f5a371d619e1', 'r', 'r', 0, 'L', '', '', 'no.jpg', ''),
('z', 'fbade9e36a3f36d3d676c1b808451dd7', '', '', 0, 'L', '', '', 'no.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE IF NOT EXISTS `user_level` (
  `level` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`level`, `nama`) VALUES
(0, 'Contributor'),
(1, 'Administrator');

-- --------------------------------------------------------

--
-- Structure for view `user`
--
DROP TABLE IF EXISTS `user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user` AS select `user_detail`.`username` AS `username`,`user_detail`.`password` AS `password`,`user_detail`.`level` AS `level` from `user_detail`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
